/**
 * Created by Marc on 19/10/2016.
 */

define([], function(){

    var api = {
        getPlayerFullName: function getPlayerFullName(player){

            return player.name.first + ' ' + player.name.last;
        },

        /**
         * Function to return an object representing the player selected from the dropdown.
         * @param player
         * @returns {{playerObject}}
         */
        getPlayerDetails: function getPlayerDetails(player){

            var playerInfoObject = {};

            // club badge
            playerInfoObject.teamBadgeImage = this.config.imagePath + this.config.teamImgPrefix + player.player.currentTeam.id + '.png';

            // picture from ID
            playerInfoObject.playerImage = this.config.imagePath + this.config.playerImgPrefix + player.player.id + '.png';

            // name, position
            playerInfoObject.playerName = api.getPlayerFullName(player.player);
            playerInfoObject.playerPosition = player.player.info.positionInfo;

            // appearances, goals, assists, goals per match, passes per minute
            playerInfoObject.appearances = player.stats.appearances.value;
            playerInfoObject.goals = player.stats.goals.value;
            playerInfoObject.assists = (player.stats.goal_assist.value >0) ? player.stats.goal_assist.value : 'No Data',
            playerInfoObject.goalsPerMatch = (player.stats.goals.value / player.stats.appearances.value ).toFixed(2);

            var totalPasses = (player.stats.backward_pass.value + player.stats.fwd_pass.value);
            playerInfoObject.passesPerMin = (totalPasses/ player.stats.mins_played.value).toFixed(2);

            return playerInfoObject;


        },

        /**
         * Function to update the UI with the selected players data.
         * @param playerObject
         */
        updateInterface: function updateInterface(playerObject) {
           var interface = {
               playerPicture: {
                   element: 'playerImage',
                   content: playerObject.playerImage,
                   type: 'image'
               },
               playerName: {
                   element: 'playerName',
                   content: playerObject.playerName,
                   type: 'html'
               },
                Position: {
                   element: 'playerPosition',
                   content: playerObject.playerPosition,
                   type: 'html'
               },
               ClubBadge: {
                   element: 'clubBadge',
                   content: playerObject.teamBadgeImage,
                   type: 'image'
               },
               Goals: {
                   element: 'Goals',
                   content: playerObject.goals,
                   type: 'html'
               },
               Appearances: {
                   element: 'Appearances',
                   content: playerObject.appearances,
                   type: 'html'
               },
               Assists: {
                   element: 'Assists',
                   content: playerObject.assists,
                   type: 'html'
               },
               GPM: {
                   element: 'GPM',
                   content: playerObject.goalsPerMatch,
                   type: 'html'
               },
               PPM: {
                   element: 'PPM',
                   content: playerObject.passesPerMin,
                   type: 'html'
               }
           };

            for(var key in interface){

                var elem = document.getElementById(interface[key].element);
                (interface[key].type === 'html') ? elem.innerHTML = interface[key].content : elem.setAttribute('src', interface[key].content)
            };
        },
        config : {
            imagePath: 'img/',
            playerImgPrefix: 'p',
            teamImgPrefix: 't',
            getRequiredStats: function(){
                return ['appearances', 'goals','goal_assist','fwd_pass','mins_played', 'backward_pass' ];
            }
        }
    };




    return api;
});

