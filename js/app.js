/**
 * Created by Marc on 19/10/2016.
 */
require(['dataProvider', 'libs'], function(API, libs) {
    // first up grab our players...
    var playersArray = API.getPlayers().players;
    var dropdown = document.getElementById('playerSelectDropDown');

    var internals = {
        /**
         * onClick handler for the dropdown
         * @param e - click event
         */
        dropDownClick: function dropDownClick(e){
            
            libs.updateInterface(libs.getPlayerDetails(playersArray[e.target.getAttribute('data-itemIndex')]));
        }
    };

    var arrayIndex = 0;
    // now iterate through them...
    playersArray.forEach(function(playerObject){

        // we need an a tag for the bootstrap dropdown to work!
        var a = document.createElement('a');

        // set the href for the a tag to nada, as we don't need it.
        a.setAttribute('href', '#');
        // Add the position within the players array that the player info is located in
        // This saves us searching for the name whens it's clicked.
        a.setAttribute('data-itemIndex', arrayIndex);

        // new list item
        var item = document.createElement('li');

        // Add our player name to the a tag
        a.appendChild(document.createTextNode(libs.getPlayerFullName(playerObject.player)));

        // add the a tag to the LI element
        item.appendChild(a);

        // we use this class later to bind the click handlers
        item.className += "playerItem";

        // Add the list item to the dropdown
        dropdown.appendChild(item);

        arrayIndex++
    });

    dropDownItems = document.getElementsByClassName('playerItem');

    for(i=0; i<dropDownItems.length; i++) {
        dropDownItems[i].addEventListener('click', internals.dropDownClick);
    }

    // Click on the first one so we get data displayed.
    dropDownItems[0].childNodes[0].click();

})();

