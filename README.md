Main App can be found at app.js, although jQuery is available I've not used any libraries(except for requireJS) as I understand pure JS is preferable. Some of the separation may seem a little overkill for this project but I wanted to show the general layout i'd use on a larger project.

Bear in mind this is a work in progress so there will be some refactoring once finished.